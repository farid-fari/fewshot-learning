from collections import defaultdict
import json

with open('../toFilter.txt') as f:
    filterWords = set(line.strip() for line in f)

translations = {word: defaultdict(int) for word in filterWords}

with open('all.prebpe.pasted') as fText,\
     open('symmetrized.align') as fAlign,\
     open('output.json', 'w') as fOut:

    for i, (lText, lAlign) in enumerate(zip(fText, fAlign)):
        if not i % 100000:
            print(i)
        lGu, lEn = lText.strip().split('\t')
        sentGu = lGu.split()
        sentEn = lEn.split()
        alignment = lAlign.strip().split()
        alignment = [(int((y := x.split('-'))[0]), int(y[1])) for x in alignment]
        for word in filterWords:
            if word in sentEn:
                j = sentEn.index(word)
                i = [sentGu[x[0]] for x in alignment if x[1] == j]
                for tra in i:
                    translations[word][tra] += 1

    json.dump(translations, fOut, sort_keys=True, indent=2, ensure_ascii=False)
