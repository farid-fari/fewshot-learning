import lzma
import pickle
import random
import torch
import transformers

BATCH_SIZE = 64
DEVICE = torch.device('cuda:0')

# MODELS
config = transformers.BertConfig.from_pretrained(
    "bert-large-uncased-whole-word-masking", output_hidden_states=True)
model = transformers.BertForMaskedLM.from_pretrained(
    "bert-large-uncased-whole-word-masking", config=config)
model = model.to(device=DEVICE)
tokenizer = transformers.BertTokenizer.from_pretrained("bert-large-uncased-whole-word-masking")

# DATA LOADING
with open("/home/arthaud/gujarati/data/parallel.filt.prebpe.en") as f:
    sents = f.readlines()

sents = [sents[pos:pos + BATCH_SIZE]
         for pos in range(0, len(sents), BATCH_SIZE)]

masked = []

for n, sentBatch in enumerate(sents):
    print(n*BATCH_SIZE)

    with torch.no_grad():
        sentBatch = tokenizer.batch_encode_plus(
            sentBatch,
            return_special_tokens_masks=True,
            pad_to_max_length=True)

    # TOKENIZING
    tokens = sentBatch['input_ids']
    special = sentBatch['special_tokens_mask']
    attentionMask = torch.tensor(sentBatch['attention_mask'], device=DEVICE)

    maskedWords = []
    for sent, spec in zip(tokens, special):
        while spec[(i := random.randint(0, len(sent) - 1))]:
            pass
        maskedWords.append(i)
    masked += maskedWords

    tokens = torch.tensor(tokens, device=DEVICE)
    maskedWords = torch.tensor(maskedWords, device=DEVICE)

    # FORWARD
    with torch.no_grad():
        out = model(tokens, attention_mask=attentionMask)[1][-1]
        out = out.cpu().numpy()
        with lzma.open('features.csv.xz', 'at') as f:
            for i in range(out.shape[0]):
                print(','.join([str(out[i, maskedWords[i], j]) for j in range(out.shape[2])]), file=f)

with open('masked.features.pickle', 'wb') as f:
    pickle.dump(masked, f)
