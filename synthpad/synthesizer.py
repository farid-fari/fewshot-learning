import heapq
import json
import lzma
import pickle
import re
import time
import torch
import transformers

BERT_MODEL = "bert-large-uncased-whole-word-masking"
DEVICE = torch.device('cuda:0')
with open('toFilter.txt') as f:
    FILTER_WORDS = set(line.strip() for line in f)
LINE_REG = re.compile(r"^([0-9+]):.*$")
# SENTS_PER_WORD = {1: 19, 2: 9, 3: 6, 5: 3, 10: 1, 15: 1}
SENTS_PER_WORD = {1: 20, 2: 20, 3: 20, 5: 20, 10: 20, 15: 20, 20: 20}
with open('alignments/output.json') as f:
    TRANSLATIONS = json.load(f)
with lzma.open('features.csv.xz', 'rt') as fFt,\
     open('data/parallel.filt.prebpe.en') as fEn,\
     open('data/parallel.filt.prebpe.gu') as fGu,\
     open('alignments/symmetrized.parallel.filt.align') as fAl,\
     open('masked.features.pickle', 'rb') as fMa:
    masked = pickle.load(fMa)
    FEATURES = [
        (torch.tensor([float(x) for x in feature.strip().split(',')]),
         lineEn.strip(), lineGu.strip(),
         [(int((y := x.split('-'))[0]), int(y[1]))
          for x in alignment.strip().split()], mask)
        for feature, lineEn, lineGu, alignment, mask
        in zip(fFt, fEn, fGu, fAl, masked)]


def tokensToWordIndexes(tokens):
    result = list(enumerate(tokens))
    firstParts = [x for x in result if x[1][:2] != '##']
    secondParts = [x for x in result if x[1][:2] == '##']

    for index, token in secondParts:
        maxIndex = 0
        for i, (a, b) in enumerate(firstParts):
            if a <= index:
                maxIndex = i

        a, b = firstParts[maxIndex]
        firstParts[maxIndex] = (a, b + token[2:])

    return firstParts


def findTopMatches(feature):
    similarity = torch.nn.CosineSimilarity(dim=0)
    result = heapq.nlargest(
        85, FEATURES, key=lambda x: similarity(x[0], feature))
    return [x[1:] for x in result]


def getTranslation(word):
    candidates = list(TRANSLATIONS[word].items())
    candidates.sort(key=lambda x: x[1], reverse=True)

    return candidates[0][0]


def synthesizeFromSent(sentEn, n, model, tokenizer):
    print(f"## {(t := time.time())} Begun sentence")
    # Tokenize
    tokenized = tokenizer.encode(sentEn)
    word_tokens = tokenizer.convert_ids_to_tokens(tokenized)

    # Find special word
    candidates = [word for word in FILTER_WORDS if word in sentEn]
    if not candidates:
        return [], []
    word = candidates[0]
    transWord = getTranslation(word)
    word_tokens = tokensToWordIndexes(word_tokens)
    candidates = [x[0] for x in word_tokens if x[1] == word.lower()]
    if not candidates:
        return [], []
    i = candidates[0]

    tokenized = torch.tensor(tokenized)
    out = model(tokenized.unsqueeze(0))
    feature = out[1][-1][0][i]
    print(f"### {time.time() - t} Feature vector computed")

    sents = findTopMatches(feature)
    print(f"### {time.time() - t} Matches found")

    resultEn, resultGu = [], []

    for englishSent, gujaratiSent, align, maskedId in sents:
        synthEn = tokenizer.convert_ids_to_tokens(
            tokenizer.encode(englishSent))

        while (maskedWord := synthEn[maskedId])[:2] == "##":
            maskedId -= 1
        k = 1
        while maskedId + k < len(synthEn) and\
                synthEn[maskedId+k].startswith('##'):
            maskedWord += synthEn[maskedId+k][2:]
            k += 1

        synthEn[maskedId:maskedId+k] = [word]
        synthEn = ' '.join(synthEn[1:-1])
        synthEn = synthEn.replace(' ##', '')

        try:
            tokenId = englishSent.lower().split().index(maskedWord)
        except ValueError:
            continue

        alignedWords = [x[1] for x in align if x[0] == tokenId]
        if not alignedWords:
            continue
        synthGu = gujaratiSent.split()
        synthGu = [w for t, w in enumerate(synthGu) if t not in alignedWords]
        synthGu.insert(min(alignedWords), transWord)
        synthGu = ' '.join(synthGu)

        resultEn.append(synthEn)
        resultGu.append(synthGu)

        if len(resultEn) >= n:
            print(f"## {time.time() - t} Sentence done")
            return resultEn, resultGu

    print(f"## {time.time() - t} Sentence done")
    return resultEn, resultGu


def main():
    # Define models
    config = transformers.BertConfig.from_pretrained(
        BERT_MODEL, output_hidden_states=True)
    model = transformers.BertForMaskedLM.from_pretrained(
        BERT_MODEL, config=config)
    tokenizer = transformers.BertTokenizer.from_pretrained(BERT_MODEL)

    for i in [1, 2, 3, 5, 10, 15, 20, ]:
        with open(f'./data/ftwords/test.{i}.tok.en') as f,\
             open(f'./data/ftwords/synthpadded/test.{i}.tok.en', 'w') as fOutEn,\
             open(f'./data/ftwords/synthpadded/test.{i}.tok.gu', 'w') as fOutGu:
            for j, line in enumerate(f):
                print(f"# Synthesizing sent {j} for size {i}")
                sentsEn, sentsGu = synthesizeFromSent(
                    line.strip(), SENTS_PER_WORD[i], model, tokenizer)
                for e in sentsEn:
                    print(e, file=fOutEn, flush=True)
                for e in sentsGu:
                    print(e, file=fOutGu, flush=True)


if __name__ == "__main__":
    main()
