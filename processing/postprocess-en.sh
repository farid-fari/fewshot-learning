#!/bin/sh

# handle arguments and usage
if [ $# -ne 2 ]; then
    echo "Usage: $0 INPUT OUPUT"
    exit
fi

thisdir=$(dirname $0)
. $thisdir/vars

input=$1
output=$2

cat $input | sed -r 's/(@@ )|(@@ ?$)//g' | \
    $moses_scripts/recaser/detruecase.perl | \
    $moses_scripts/tokenizer/detokenizer.perl -l en > $output
