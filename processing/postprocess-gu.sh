#!/bin/sh

# handle arguments and usage
if [ $# -ne 2 ]; then
    echo "Usage: $0 INPUT OUPUT"
    exit
fi

thisdir=$(dirname $0)
. $thisdir/vars

infile=$1
outfile=$2

# de-BPE
cat $infile | perl -pe 's/@@ @-@ /@@ -/g' | \
    sed -r 's/(@@ )|(@@ ?$)//g' | \
    $moses_scripts/tokenizer/detokenizer.perl -l gu > $infile.debpe.detok-moses

python3 \
	  $indicnlplibrary/tokenize/indic_detokenize.py \
	  $infile.debpe.detok-moses $outfile gu

# remove intermediate file
rm $infile.debpe.detok-moses
