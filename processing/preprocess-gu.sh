#!/bin/sh

# handle arguments and usage
if [ $# -ne 2 ]; then
    echo "Usage: $0 INPUT OUPUT"
    exit
fi
input=$1
output=$2

thisdir=$(dirname $0)
. $thisdir/vars # contains variables to tools

# language-specific parameters
lang=gu
bpemodel=$thisdir/../preproc_models/guen.bpe


# apply normalisation using indicnlp library and moses
python3 \
	  $indicnlplibrary/normalize/indic_normalize.py \
	  $input $output.norm $lang
cat $output.norm | \
    $moses_scripts/tokenizer/normalize-punctuation.perl \
	-l $lang \
	> $output.tok1

# tokenise using indicnlp library
python3 \
	  $indicnlplibrary/tokenize/indic_tokenize.py \
	  $output.tok1 $output.tok $lang

# apply bpe using fastbpe
$fastbpe/fast applybpe $output $output.tok $bpemodel

# remove intermediate files
#rm $output.tok
rm $output.tok1
rm $output.norm
