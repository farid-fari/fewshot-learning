#!/bin/sh

# handle arguments and usage
if [ $# -ne 2 ]; then
    echo "Usage: $0 INPUT OUPUT"
    exit
fi
input=$1
output=$2

thisdir=$(dirname $0)
. $thisdir/vars # contains variables to tools

# language-specific parameters
lang=en
bpemodel=$thisdir/../preproc_models/guen.bpe
truecasemodel=/mnt/vali0/www/data.statmt.org/wmt19_systems/preproc_models/truecase-model.en #$thisdir/../preproc_models/truecase-model.$lang

# normalisation, tokenisation and truecasing
cat $input | \
    $moses_scripts/tokenizer/normalize-punctuation.perl -l $lang | \
    $moses_scripts/tokenizer/tokenizer.perl -a -l $lang | \
    $moses_scripts/recaser/truecase.perl -model $truecasemodel \
					 > $output.tok

# fastBPE
$fastbpe/fast applybpe $output $output.tok $bpemodel


# remove intermediate files
rm $output.tok


#cat en-gu/backtrans.en-gu.tok.tc.cleaned.filtered.gu > forbpe
#cat en-gu/parallel.tok.tc.clean.* >> forbpe
#cat gu-en/backtrans.gu-en.tok.tc.cleaned.filtered.en | shuf | head -n4971894 >> forbpe
