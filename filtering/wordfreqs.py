from collections import defaultdict
import nltk
from nltk.tag import pos_tag
from nltk.chunk import tree2conlltags
import subprocess

namedEntities = set()
testOccs = defaultdict(int)
trainingOccs = defaultdict(int)
transOccs = defaultdict(int)
accuracies = {}

pattern = 'PN: {(<NNP>|<NNPS>)*}'
re = nltk.RegexpParser(pattern)


def lineToWords(l):
    l = l.strip().split()
    return l

    l = pos_tag(l)
    l = re.parse(l)
    l = tree2conlltags(l)

    final = []
    cur = []

    for w, _, t in l:
        if len(w) > 1 and t.endswith('PN'):
            final.append(w)

            if t == 'I-PN':
                cur.append(w)
            elif t == 'B-PN':
                if len(cur) > 1:
                    final.append(' '.join(cur))
                cur = [w]
    if len(cur) > 1:
        final.append(' '.join(cur))

    return final


# TEST REFERENCE
for l in open('/home/arthaud/gujarati/dev_and_test_data/total.tok.en'):
    for w in lineToWords(l):
        namedEntities.add(w)
        testOccs[w] += 1

print(len(namedEntities), "unique named entities found.")
print(sum(testOccs.values()), "total occurences in test set.\n")

namedEntities = {e for e in namedEntities if testOccs[e] >= 5}

print(len(namedEntities), "unique [over 5 occ] named entities found.")

# TRAIN REFERENCE
for w in namedEntities:
    print('"', w, '":', sep='', end=' ', flush=True)
    p1 = subprocess.Popen(['grep', '-wioe', w, '/home/arthaud/gujarati/data/train.total.tok'], stdout=subprocess.PIPE)
    result = subprocess.Popen(['wc', '-l'], stdin=p1.stdout, stdout=subprocess.PIPE)
    p1.stdout.close()
    result = result.communicate()[0]
    result = result.decode('utf-8')
    trainingOccs[w] = int(result)
    print(int(result))

print("\n\n", sum(trainingOccs.values()), " total occurences in test set.", sep='')

for w in namedEntities:
    print(trainingOccs[w], w, testOccs[w], sep='\t')
