with open('toFilter.txt') as f:
    filterWords = set(line.strip().lower() for line in f)

print(len(filterWords))

with open('/home/arthaud/gujarati/data/parallel.tok.en') as fEn,\
     open('/home/arthaud/gujarati/data/parallel.bpe.gu') as fGu,\
     open('/home/arthaud/gujarati/data/parallel.filt.tok.en', 'w') as foEn,\
     open('/home/arthaud/gujarati/data/parallel.filt.bpe.gu', 'w') as foGu,\
     open('/home/arthaud/gujarati/data/parallelFilteredOut.tok.en', 'w') as foEnFi,\
     open('/home/arthaud/gujarati/data/parallelFilteredOut.bpe.gu', 'w') as foGuFi:

    for lEn, lGu in zip(fEn, fGu):
        if any(w in lEn.strip().lower().split() for w in filterWords):
            print(lEn.strip(), file=foEnFi)
            print(lGu.strip(), file=foGuFi)
        else:
            print(lEn.strip(), file=foEn)
            print(lGu.strip(), file=foGu)
