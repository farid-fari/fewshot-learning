import numpy as np
from random import shuffle

with open('toFilter.txt') as f:
    toFilter = [line.strip() for line in f]

currentEn, currentGu = [], []
currentCount = np.zeros(len(toFilter))

counts = [1, 2, 3, 5, 10, 15, 20]


def countInSent(s):
    counts = np.zeros(len(toFilter))
    for i, w in enumerate(toFilter):
        counts[i] += s.count(w)
    return counts


for c in counts:
    with open('/home/arthaud/gujarati/data/filteredOut.tok.en') as fE,\
         open('/home/arthaud/gujarati/data/filteredOut.bpe.gu') as fG:
        pasted = list(zip(fE.readlines(), fG.readlines()))

    prioritySat = set()
    saved = (currentEn.copy(), currentGu.copy(), currentCount.copy())
    while (currentCount != c).any():
        currentEn, currentGu, currentCount = saved
        saved = (currentEn.copy(), currentGu.copy(), currentCount.copy())
        shuffle(pasted)

        print(f"Priority: {prioritySat}.")

        # PriorityRun
        if prioritySat:
            for lE, lG in pasted:
                lE, lG = lE.strip(), lG.strip()
                if lE not in currentEn and ((cts := countInSent(lE)) + currentCount <= c).all():
                    if not set(cts.nonzero()[0]).isdisjoint(prioritySat):
                        currentCount += cts
                        currentEn.append(lE.strip())
                        currentGu.append(lG.strip())

        shuffle(pasted)

        for lE, lG in pasted:
            lE, lG = lE.strip(), lG.strip()
            if (lE not in currentEn and ((cts := countInSent(lE)) + currentCount <= c).all()):
                currentCount += cts
                currentEn.append(lE.strip())
                currentGu.append(lG.strip())

        prioritySat = set((currentCount < c).nonzero()[0])

    print(f"Satisfied {c}.")
    with open(f"test.{c}.tok.en", "w") as fE, open(f"test.{c}.bpe.gu", 'w') as fG:
        for lE, lG in zip(currentEn, currentGu):
            print(lE, file=fE)
            print(lG, file=fG)
