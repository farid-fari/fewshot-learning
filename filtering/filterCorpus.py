with open('toFilter.txt') as f:
    filterWords = set(line.strip().lower() for line in f)

print(len(filterWords))

with open('/home/arthaud/gujarati/data/all.unfilt.tok.en') as fEn,\
     open('/home/arthaud/gujarati/data/all.unfilt.bpe.gu') as fGu,\
     open('/home/arthaud/gujarati/data/all.filt.tok.en', 'w') as foEn,\
     open('/home/arthaud/gujarati/data/all.filt.bpe.gu', 'w') as foGu,\
     open('/home/arthaud/gujarati/data/filteredOut.tok.en', 'w') as foEnFi,\
     open('/home/arthaud/gujarati/data/filteredOut.bpe.gu', 'w') as foGuFi:

    for lEn, lGu in zip(fEn, fGu):
        if any(w in lEn.strip().lower().split() for w in filterWords):
            print(lEn.strip(), file=foEnFi)
            print(lGu.strip(), file=foGuFi)
        else:
            print(lEn.strip(), file=foEn)
            print(lGu.strip(), file=foGu)
