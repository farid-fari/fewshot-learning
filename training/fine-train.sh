#!/bin/bash

thisdir=$(dirname $0)
. $thisdir/vars

GPUS=$@

# fine-tuning model
modeldir=$thisdir/model-$seed-finetune
mkdir -p $modeldir

# get best model from previous training and copy over
#ITER=$( cat $thisdir/model-$seed/validation/iter.log | sort -k2 -nr | cut -d' ' -f1 | head -n1 )
ITER=130000
echo "Training from the previous model at iteration $ITER"
cp $thisdir/model-$seed/model.iter$ITER.npz $modeldir/old_model.npz


$MARIAN/build/marian \
    --seed $seed \
    --devices $GPUS \
    --pretrained-model $modeldir/old_model.npz \
    --model $modeldir/model.npz \
    --type transformer \
    --train-sets $parallel_train_src $parallel_train_trg \
    --max-length 120 \
    --vocabs $vocab_src $vocab_trg \
    --mini-batch-fit \
    -w 10000 \
    --maxi-batch 80 \
    --early-stopping 6 \
    --cost-type ce-mean-words \
    --valid-freq 250 \
    --save-freq 250 \
    --disp-freq 50 \
    --log $modeldir/train.log \
    --valid-log $modeldir/valid.log \
    --valid-metrics ce-mean-words perplexity translation  \
    --valid-sets $dev_src $dev_trg \
    --valid-translation-output $modeldir/valid.output \
    --valid-mini-batch 16 \
    --valid-max-length 80 \
    --beam-size 6 \
    --normalize 1 \
    --enc-depth 6 \
    --dec-depth 6 \
    --transformer-heads 8 \
    --transformer-postprocess-emb d \
    --transformer-postprocess dan \
    --transformer-dropout 0.1 \
    --label-smoothing 0.1 \
    --learn-rate 0.0003 \
    --lr-warmup 16000 \
    --lr-decay-inv-sqrt 16000 \
    --lr-report \
    --optimizer-params 0.9 0.98 1e-09 \
    --clip-norm 5 \
    --sync-sgd \
    --exponential-smoothing \
    --valid-script-path /home/arthaud/gujarati/farid-experiments/validate.sh \
    --optimizer-delay 2

