#!/bin/bash

thisdir=$(dirname $0)
. $thisdir/vars

# fine-tuning model
modeldir=$thisdir/model.$2.$3.$4.ftwords
mkdir -p $modeldir

$MARIAN/build/marian \
    --seed $seed \
    --devices $1 \
    -e $3 \
    --pretrained-model $thisdir/model-1-finetune/model.best.npz \
    --model $modeldir/model.npz \
    --type transformer \
    --train-sets /home/arthaud/gujarati/data/ftwords/test.$2.bpe.{gu,en} \
    --max-length 520 \
    --vocabs $vocab_src $vocab_trg \
    --mini-batch 32 \
    -w 9000 \
    --maxi-batch 1 \
    --maxi-batch-sort none \
    --cost-type ce-mean-words \
    --disp-freq 5 \
    --save-freq 3820 \
    --log $modeldir/train.log \
    --valid-log $modeldir/valid.log \
    --beam-size 6 \
    --normalize 1 \
    --enc-depth 6 \
    --dec-depth 6 \
    --transformer-heads 8 \
    --transformer-postprocess-emb d \
    --transformer-postprocess dan \
    --transformer-dropout 0.1 \
    --label-smoothing 0.1 \
    -o adam \
    --learn-rate 0.$4 \
    --lr-decay 0 \
    --lr-report \
    --sync-sgd \
    --exponential-smoothing
