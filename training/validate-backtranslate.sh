#!/bin/bash

thisdir=$(dirname $0)
. $thisdir/vars

export LC_ALL=C.UTF-8

cat $1 \
    | perl -pe 's/@@ @-@ /@@ -/g' \
    | sed -r 's/(@@ )|(@@ ?$)//g' \
    | $MOSESSCRIPTS/tokenizer/detokenizer.perl -l $src \
    | $MOSESSCRIPTS/generic/multi-bleu-detok.perl $ref_back \
    | sed -r 's/BLEU = ([0-9.]+),.*/\1/'
