#!/bin/bash

thisdir=$(dirname $0)
. $thisdir/vars

export LC_ALL=C.UTF-8

cat $1 \
    | sed 's/\@\@ //g' \
    | $MOSESSCRIPTS/recaser/detruecase.perl  \
    | $MOSESSCRIPTS/tokenizer/detokenizer.perl -l $trg  \
    | $MOSESSCRIPTS/generic/multi-bleu-detok.perl $ref \
    | sed -r 's/BLEU = ([0-9.]+),.*/\1/'
