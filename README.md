# Few-shot learning through contextual data augmentation

This repository contains the code associated to the EACL 2021 paper by Farid
Arthaud, Rachel Bawden and Alexandra Birch.

Scripts are generally organized in the following way,
- `processing/` contains scripts to pre and post-process parallel data
- `filtering/` contains scripts to select rarely-used words and filter
  sentences containing them from the data
- `training/` contains scripts used to train the different models
- `synthpad/` contains scripts used to create synthetic sentences

## Usage
After setting the variables in `processing/vars`, the preprocessing scripts in
English and Gujarati should be used to process the test and training test. If
using another language pair, appropriate preprocessing and postprocessing
scripts should be used.
The variables include an `indicnlp` install which should be used, `moses`
scripts, `fastbpe` and `marian`.

Next, the `wordfreqs.py` script is used to establish the list of words by usage
in the training and test set. Edit the script to point to the appropriate
location of these files, and pipe the output to a file which should then
manually be edited down to the rare words to be used in the simulated setup.
This file is named `toFilter.txt` by default in the following scripts.

The `filterParallel.py` and `filterCorpus.py` scripts allow to filter the
various files by removing those that contain the evaluation words. These
scripts should be edited to point to the appropriate files, and read a list of
evaluation words from `toFilter.txt`.

Then, `buildCorpus.py` will build files containing 1 to 20 occurrences of each
word using the held-out sentences. It should be edited to point to the
appropriate file containing the held-out sentences.

Edit `training/training-vars` to point to the appropriate data files,
vocabulary files, and `marian` install.
Then, `train.sh` will train the main model on the held-out data.
Then, run `fine-train.sh` to fine-tune said model on legitimate parallel data
and name your best checkpoint `model.best.npz`.

Then head to `synthpad` to first run alignment using commands similar to those
in `synthpad/align.sh` to create alignments for the entire filtered training
set.
Do this for the unfiltered training set as well and then run `findAligned.py`
to generate translations for the evaluation words for future substitutions.

Next, run `bertProps.py` to randomly mask a random word in each sentence in the
filtered training set and generate context vectors using huggingface's BERT
model.
Then, `synthesizer.py` will generate synthetic sentences by computing the top
matches, running alignment and making substitutions as described in the main
paper.
This will require the alignements and translations for evaluation words
generated in the previous step.

In `training`, the scripts `words-train.sh`, `words-synthpadded-train.sh`,
`words-randompad-train.sh` and `words-half-train.sh` allow to train the
*finetune*, *augmented*, *randompad* and *half* approaches respectively from
the best model.
You can then use these models to create translations as required, and use the
scripts in `processing` to post-process their output if you used the
preprocessing scripts from there.

## Reference
If you use this code, please cite,

> *Few-shot learning through contextual data augmentation.* Farid Arthaud,
> Rachel Bawden and Alexandra Birch. 2021. Proceedings of the 16th Conference
> of the European Chapter of the Association for Computational Linguistics:
> Volume 1, Long Papers. Online.

```
@inproceedings{arthaud_fewshot_2021,
  title = “Few-shot learning through contextual data augmentation”,
  author = “Arthaud, Farid and Bawden, Rachel and Birch, Alexandra”,
  booktitle = “Proceedings of the 16th Conference of the {E}uropean Chapter of the Association for Computational Linguistics: Volume 1, Long Papers”,
  year = “2021",
  address = “Online”
}
```

## Authors
For any question, the authors can be contacted at farid.arthaud "at" ens.psl.eu

## License
The code is distributed under the MIT license like the Marian software.
